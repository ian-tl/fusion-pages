webpackJsonp([3,5],{

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(471)(__webpack_require__(434))

/***/ }),

/***/ 434:
/***/ (function(module, exports) {

module.exports = "(function () {\n  function setCookie(cname, cvalue, exdays) {\n    const d = new Date();\n    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));\n    const expires = `expires=${d.toUTCString()}`;\n    document.cookie = `${cname}=${cvalue};${expires};path=/`;\n  }\n\n  function getCookie(cname) {\n    const name = `${cname}=`;\n    const decodedCookie = decodeURIComponent(document.cookie);\n    const ca = decodedCookie.split(';');\n\n    for (let i = 0; i < ca.length; i++) {\n      let c = ca[i];\n      while (c.charAt(0) == ' ') {\n        c = c.substring(1);\n      }\n\n      if (c.indexOf(name) == 0) {\n        return c.substring(name.length, c.length);\n      }\n    }\n\n    return '';\n  }\n\n  const $body = $('body, html');\n  const $tabsList = $('.tabs__list');\n  const $tabs = $('.tabs__tab');\n  const $activeTabLabel = $('.tabs__active-tab');\n  const $regionDestContainer = $('.region-destinations');\n  const $weatherIcons = $body.find('.weather-icon');\n  const $unlockNowForm = $body.find('.unlock-now__form');\n\n  if (getCookie('unlockNow')) {\n    $unlockNowForm.parents('.unlock-now').remove();\n  }\n\n  const skycons = new Skycons({ color: 'black' });\n\n  const toggleTab = function (evt) {\n    evt.stopPropagation();\n    evt.preventDefault();\n    $tabsList.toggleClass('tabs__list--is-open');\n  };\n\n  const gotoTab = function (evt) {\n    evt.stopPropagation();\n    evt.preventDefault();\n    const $link = $(evt.currentTarget);\n    const $targetBlock = $body.find($link.attr('href'));\n\n    if (!$targetBlock.length) {\n      return;\n    }\n\n    const title = $link.data('tab-title');\n    $tabs.removeClass('tabs__tab--is-active');\n    $link.parent().addClass('tabs__tab--is-active');\n    $activeTabLabel.text(title);\n    $tabsList.toggleClass('tabs__list--is-open');\n\n    $body.animate({\n      scrollTop: $body.find($link.attr('href')).offset().top\n    }, 400);\n  };\n\n\n  const toggleAccordion = function (evt) {\n    evt.stopPropagation();\n    evt.preventDefault();\n    const $link = $(evt.currentTarget);\n    const $accordionContainer = $link.parents('.link-accordion');\n    $accordionContainer.toggleClass('link-accordion--is-open');\n  };\n\n  const toggleNavMenu = function (evt) {\n    evt.stopPropagation();\n    evt.preventDefault();\n    $body.toggleClass('nav--is-open');\n  };\n\n  const toggleRegionDestinations = function (evt) {\n    evt.stopPropagation();\n    evt.preventDefault();\n    $regionDestContainer.toggleClass('region-destinations--is-open');\n    $body.animate({\n      scrollTop: $regionDestContainer.offset().top\n    }, 400);\n  };\n\n  const dismissUnlockNowBar = function () {\n    $unlockNowForm.parents('.unlock-now').remove();\n    setCookie('unlockNow', 'hidden', 30);\n  };\n\n  const submitUnlockNow = function (evt) {\n    evt.stopPropagation();\n    evt.preventDefault();\n    const $form = $(this);\n    const formData = $form.serialize();\n\n    $.ajax({\n      url: 'https://www.travelodge.co.uk/api/v1/subscription',\n      method: $form.attr('method'),\n      data: formData\n    }).then((data, textStatus, jqXHR) => {\n      $unlockNowForm.html('<h3>Thank you for subcribing!</h3>');\n      setTimeout(() => {\n        dismissUnlockNowBar();\n      }, 3000);\n    }, (jqXHR, textStatus, errorThrown) => {\n      if (jqXHR.status === 200 || jqXHR.status === 302) {\n        $unlockNowForm.html('<h3>Thank you for subcribing!</h3>');\n        setTimeout(function () {\n          dismissUnlockNowBar();\n        }, 3000);\n\n        return;\n      }\n\n      $form.find('input')\n        .attr('placeholder', 'Enter a valid email')\n        .val('')\n        .addClass('has-errors')\n        .focus();\n    });\n  };\n\n  $body\n    .on('click', '.nav__hamburger, .mobile-nav__close-btn', toggleNavMenu)\n    .on('click', '.link-accordion__toggle-btn', toggleAccordion)\n    .on('click', '.tab__link', gotoTab)\n    .on('click', '.region-destinations__toggle', toggleRegionDestinations)\n    .on('click', '.unlock-now__dismiss-btn', dismissUnlockNowBar)\n    .on('click', '#tab-toggle', toggleTab);\n\n  $unlockNowForm.on('submit', submitUnlockNow);\n\n  $weatherIcons.each((index, element) => {\n    const iconType = $weatherIcons.eq(index).data('icon-type');\n    skycons.add(element, iconType);\n  });\n\n  skycons.play();\n}());\n"

/***/ }),

/***/ 471:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
module.exports = function(src) {
	function log(error) {
		(typeof console !== "undefined")
		&& (console.error || console.log)("[Script Loader]", error);
	}

	// Check for IE =< 8
	function isIE() {
		return typeof attachEvent !== "undefined" && typeof addEventListener === "undefined";
	}

	try {
		if (typeof execScript !== "undefined" && isIE()) {
			execScript(src);
		} else if (typeof eval !== "undefined") {
			eval.call(null, src);
		} else {
			log("EvalError: No eval function available");
		}
	} catch (error) {
		log(error);
	}
}


/***/ }),

/***/ 495:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(262);


/***/ })

},[495]);
//# sourceMappingURL=scripts.bundle.js.map
webpackJsonp([1,5],{

/***/ 258:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 258;


/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(275);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_bar_search_bar_component__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__location_field_location_field_component__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__date_fields_date_fields_component__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__select_select_component__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modal_dialog_modal_dialog_component__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__datepicker_datepicker_component__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__rooms_fields_rooms_fields_component__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__rooms_rooms_component__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__room_room_component__ = __webpack_require__(270);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__search_bar_search_bar_component__["a" /* SearchBarComponent */],
            __WEBPACK_IMPORTED_MODULE_5__location_field_location_field_component__["a" /* LocationFieldComponent */],
            __WEBPACK_IMPORTED_MODULE_6__date_fields_date_fields_component__["a" /* DateFieldsComponent */],
            __WEBPACK_IMPORTED_MODULE_7__select_select_component__["a" /* SelectComponent */],
            __WEBPACK_IMPORTED_MODULE_8__modal_dialog_modal_dialog_component__["a" /* ModalDialogComponent */],
            __WEBPACK_IMPORTED_MODULE_9__datepicker_datepicker_component__["a" /* DatepickerComponent */],
            __WEBPACK_IMPORTED_MODULE_10__rooms_fields_rooms_fields_component__["a" /* RoomsFieldsComponent */],
            __WEBPACK_IMPORTED_MODULE_11__rooms_rooms_component__["a" /* RoomsComponent */],
            __WEBPACK_IMPORTED_MODULE_12__room_room_component__["a" /* RoomComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* ReactiveFormsModule */]
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__search_bar_search_bar_component__["a" /* SearchBarComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_dialog_modal_dialog_component__ = __webpack_require__(49);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateFieldsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var DATE_FORMAT = 'DD/MM/YYYY';
var TABLET_WIDTH = 768;
var DateFieldsComponent = (function () {
    function DateFieldsComponent(parentComponent) {
        var _this = this;
        this.parentComponent = parentComponent;
        this.isMobile = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]();
        if (this.parentComponent) {
            this.parentComponent.visibleChange.subscribe(function (modalVisible) {
                if (modalVisible) {
                    _this.checkInComponent.api.open();
                }
            });
        }
    }
    DateFieldsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.parentForm.get('checkIn').valueChanges.subscribe(function (checkIn) {
            var newCheckOut = __WEBPACK_IMPORTED_MODULE_2_moment__(checkIn, DATE_FORMAT).add(_this.parentForm.get('nights').value, 'd');
            _this.parentForm.get('checkOut').setValue(newCheckOut.format(DATE_FORMAT));
            _this.checkOutComponent.api.open();
        });
    };
    DateFieldsComponent.prototype.checkDeviceWidth = function (evt) {
        if (window.innerWidth <= TABLET_WIDTH) {
            this.isMobile.emit();
        }
    };
    return DateFieldsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('checkInComponent'),
    __metadata("design:type", Object)
], DateFieldsComponent.prototype, "checkInComponent", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('checkOutComponent'),
    __metadata("design:type", Object)
], DateFieldsComponent.prototype, "checkOutComponent", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])('parentForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */]) === "function" && _a || Object)
], DateFieldsComponent.prototype, "parentForm", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Output */])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]) === "function" && _b || Object)
], DateFieldsComponent.prototype, "isMobile", void 0);
DateFieldsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-date-fields',
        template: __webpack_require__(435)
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* Optional */])()),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__modal_dialog_modal_dialog_component__["a" /* ModalDialogComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__modal_dialog_modal_dialog_component__["a" /* ModalDialogComponent */]) === "function" && _c || Object])
], DateFieldsComponent);

var _a, _b, _c;
//# sourceMappingURL=date-fields.component.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_dialog_modal_dialog_component__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var $ = window['$'];
var DATE_FORMAT = 'dd/mm/yy';
var MOMENT_DATE_FORMAT = 'DD/MM/YYYY';
var TABLET_WIDTH = 768;
var DatepickerComponent = (function () {
    function DatepickerComponent(parentComponent) {
        var _this = this;
        this.parentComponent = parentComponent;
        this.onChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]();
        this.api = {
            open: this.open.bind(this),
            close: this.close.bind(this)
        };
        this.isOpen = false;
        this.datePickerConfig = {
            dateFormat: DATE_FORMAT,
            showOtherMonths: true,
            selectOtherMonths: true,
            beforeShowDay: function (date) {
                if (_this.controlName === 'checkOut') {
                    var checkIn = __WEBPACK_IMPORTED_MODULE_3_moment__(_this.parentForm.get('checkIn').value, MOMENT_DATE_FORMAT);
                    var checkOut = __WEBPACK_IMPORTED_MODULE_3_moment__(_this.parentForm.get('checkOut').value, MOMENT_DATE_FORMAT);
                    if (checkIn.isSame(date)) {
                        return [false, 'highlight'];
                    }
                    if (checkIn.isBefore(date) && checkOut.isSameOrAfter(date)) {
                        return [true, 'highlight'];
                    }
                }
                return [true, ''];
            }
        };
        document.addEventListener('click', this.offClickHandler.bind(this));
    }
    DatepickerComponent.prototype.ngOnInit = function () {
        var $container = $(this.container.nativeElement);
        var config = Object.assign({}, this.datePickerConfig, {
            minDate: this.minDate,
            defaultDate: this.inputValue,
            onSelect: this.onSelect.bind(this)
        });
        $container.find('.datepicker-holder').datepicker(config);
    };
    DatepickerComponent.prototype.ngOnChanges = function (changes) {
        if (changes['defaultDate'] && !changes['defaultDate'].firstChange) {
            $(this.datepicker.nativeElement).datepicker('setDate', this.defaultDate);
        }
        if (changes['minDate'] && !changes['minDate'].firstChange && this.controlName === 'checkOut') {
            $(this.datepicker.nativeElement).datepicker('option', 'minDate', this.minDate);
        }
        if (this.controlName === 'checkOut') {
            $(this.datepicker.nativeElement).datepicker('setDate', this.defaultDate);
        }
    };
    DatepickerComponent.prototype.offClickHandler = function (event) {
        if (!this.container.nativeElement.contains(event.target)) {
            this.close();
        }
    };
    DatepickerComponent.prototype.onSelect = function (value) {
        this.parentForm.get(this.controlName).setValue(value);
        this.close();
    };
    DatepickerComponent.prototype.close = function () {
        this.isOpen = false;
    };
    DatepickerComponent.prototype.open = function () {
        this.isOpen = true;
    };
    DatepickerComponent.prototype.onFocus = function (evt) {
        if (window.innerWidth <= TABLET_WIDTH && !this.parentComponent) {
            return false;
        }
        this.open();
    };
    DatepickerComponent.prototype.setCheckInDate = function (value) {
        var checkIn = __WEBPACK_IMPORTED_MODULE_3_moment__().add(value, 'days');
        this.parentForm.get('checkIn').setValue(checkIn.format(MOMENT_DATE_FORMAT));
        this.close();
    };
    DatepickerComponent.prototype.setCheckOutDate = function (value) {
        var checkIn = __WEBPACK_IMPORTED_MODULE_3_moment__(this.parentForm.get('checkIn').value, MOMENT_DATE_FORMAT);
        var checkOut = __WEBPACK_IMPORTED_MODULE_3_moment__(checkIn).add(value, 'days');
        this.parentForm.get('checkOut').setValue(checkOut.format(MOMENT_DATE_FORMAT));
        this.close();
    };
    DatepickerComponent.prototype.checkInEquals = function (value) {
        var checkIn = __WEBPACK_IMPORTED_MODULE_3_moment__(this.parentForm.get('checkIn').value, MOMENT_DATE_FORMAT).startOf('day');
        var comparison = __WEBPACK_IMPORTED_MODULE_3_moment__().startOf('day').add(value, 'days');
        return comparison.isSame(checkIn);
    };
    DatepickerComponent.prototype.nightsEqual = function (value) {
        return this.parentForm.get('nights').value === value;
    };
    return DatepickerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('container'),
    __metadata("design:type", Object)
], DatepickerComponent.prototype, "container", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('datepicker'),
    __metadata("design:type", Object)
], DatepickerComponent.prototype, "datepicker", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])('parentForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */]) === "function" && _a || Object)
], DatepickerComponent.prototype, "parentForm", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", String)
], DatepickerComponent.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", String)
], DatepickerComponent.prototype, "className", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", String)
], DatepickerComponent.prototype, "defaultDate", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", String)
], DatepickerComponent.prototype, "minDate", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", String)
], DatepickerComponent.prototype, "inputValue", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", String)
], DatepickerComponent.prototype, "controlName", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], DatepickerComponent.prototype, "config", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Output */])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]) === "function" && _b || Object)
], DatepickerComponent.prototype, "onChange", void 0);
DatepickerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-datepicker',
        template: __webpack_require__(436)
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* Optional */])()),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__modal_dialog_modal_dialog_component__["a" /* ModalDialogComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__modal_dialog_modal_dialog_component__["a" /* ModalDialogComponent */]) === "function" && _c || Object])
], DatepickerComponent);

var _a, _b, _c;
//# sourceMappingURL=datepicker.component.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_modal_dialog_modal_dialog_component__ = __webpack_require__(49);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationFieldComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var $ = window['$'];
var typeahead = $.fn.typeahead;
var Bloodhound = window['Bloodhound'];
var bearhug = window['bearhug'];
var LocationFieldComponent = (function () {
    function LocationFieldComponent(parentComponent) {
        var _this = this;
        this.parentComponent = parentComponent;
        if (this.parentComponent) {
            this.parentComponent.visibleChange.subscribe(function (modalVisible) {
                if (modalVisible) {
                    _this.setupTypeahead();
                }
            });
        }
    }
    LocationFieldComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setupTypeahead();
        this.parentForm.get('location').valueChanges.subscribe(function (value) {
            _this.parentForm.get('lat').setValue('');
            _this.parentForm.get('long').setValue('');
        });
    };
    LocationFieldComponent.prototype.setupTypeahead = function () {
        var _this = this;
        var $input = $(this.input.nativeElement);
        $input.typeahead('destroy');
        var terms = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'https://www.travelodge.co.uk/search_and_book/places_json.php?stop_mobi=yes&searchString=%QUERY',
                wildcard: '%QUERY',
                transform: function (response) {
                    return response.suggestions;
                },
            }
        });
        setTimeout(function () {
            if (_this.parentComponent && _this.parentComponent.container) {
                $input = $(_this.parentComponent.container.nativeElement).find('input#location');
            }
        });
        $input.typeahead({
            minLength: 3,
            hint: false,
            highlight: true,
        }, {
            name: 'search',
            display: 'value',
            source: terms,
            limit: 6,
            templates: {
                suggestion: function (suggestion) {
                    var template = $('<div/>').text(suggestion.value);
                    bearhug({
                        node: template[0],
                        pattern: [suggestion._query]
                    });
                    if (suggestion.value.indexOf(' Travelodge') !== -1) {
                        template.addClass('tl-suggestion');
                    }
                    return template;
                }
            }
        }).on('typeahead:select', function (evt, suggestion) {
            _this.parentForm.get('location').setValue(suggestion.value);
            _this.parentForm.get('lat').setValue('');
            _this.parentForm.get('long').setValue('');
            $input.typeahead('close');
        });
    };
    LocationFieldComponent.prototype.errorHandler = function (err) {
        if (err.code === 1) {
            alert('Denied location access request. To enable, please go to Settings and turn on Location Service ' +
                'for this app.');
        }
        else if (err.code === 2) {
            alert('Error: Position is unavailable!');
        }
    };
    LocationFieldComponent.prototype.showLocation = function (position) {
        var _a = position.coords, longitude = _a.longitude, latitude = _a.latitude;
        this.parentForm.get('location').setValue('Use current location');
        this.parentForm.get('lat').setValue(latitude);
        this.parentForm.get('long').setValue(longitude);
    };
    LocationFieldComponent.prototype.setCurrentLocation = function () {
        if (navigator.geolocation) {
            var options = {
                timeout: 60000
            };
            // timeout at 60000 milliseconds (60 seconds)
            navigator.geolocation.getCurrentPosition(this.showLocation.bind(this), this.errorHandler, options);
        }
        else {
            alert('Sorry, your browser does not support location search.');
        }
    };
    LocationFieldComponent.prototype.clearInput = function () {
        this.parentForm.get('location').setValue('');
        this.parentForm.get('lat').setValue('');
        this.parentForm.get('long').setValue('');
    };
    LocationFieldComponent.prototype.onLocationBlur = function (evt) {
        var _this = this;
        if (this.parentForm.get('location').value.trim() === '') {
            setTimeout(function () {
                _this.parentForm.get('location').setValue('');
            }, 1);
        }
    };
    return LocationFieldComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('input'),
    __metadata("design:type", Object)
], LocationFieldComponent.prototype, "input", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])('parentForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */]) === "function" && _a || Object)
], LocationFieldComponent.prototype, "parentForm", void 0);
LocationFieldComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-location-field',
        template: __webpack_require__(437)
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* Optional */])()),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_app_modal_dialog_modal_dialog_component__["a" /* ModalDialogComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_app_modal_dialog_modal_dialog_component__["a" /* ModalDialogComponent */]) === "function" && _b || Object])
], LocationFieldComponent);

var _a, _b;
//# sourceMappingURL=location-field.component.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__select_select_component__ = __webpack_require__(85);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoomComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RoomComponent = (function () {
    function RoomComponent() {
        this.adultOptions = [
            new __WEBPACK_IMPORTED_MODULE_2__select_select_component__["b" /* Option */](1, '1 adult'),
            new __WEBPACK_IMPORTED_MODULE_2__select_select_component__["b" /* Option */](2, '2 adults'),
            new __WEBPACK_IMPORTED_MODULE_2__select_select_component__["b" /* Option */](3, '3 adults'),
        ];
        this.defaultChildrenOptions = [
            new __WEBPACK_IMPORTED_MODULE_2__select_select_component__["b" /* Option */](0, '0 children'),
            new __WEBPACK_IMPORTED_MODULE_2__select_select_component__["b" /* Option */](1, '1 child'),
            new __WEBPACK_IMPORTED_MODULE_2__select_select_component__["b" /* Option */](2, '2 children'),
        ];
        this.childrenOptions = this.defaultChildrenOptions;
    }
    RoomComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.room.get('adults').valueChanges
            .subscribe(function () { return _this.updateChildOptions(); });
    };
    RoomComponent.prototype.updateChildOptions = function () {
        var adultCount = this.room.get('adults').value;
        var childCount = this.room.get('children').value;
        if (adultCount >= 3) {
            this.childrenOptions = [
                new __WEBPACK_IMPORTED_MODULE_2__select_select_component__["b" /* Option */](0, '0 children'),
                new __WEBPACK_IMPORTED_MODULE_2__select_select_component__["b" /* Option */](1, '1 child'),
            ];
            if (childCount >= 2) {
                this.room.get('children').setValue(0);
            }
        }
        else {
            this.childrenOptions = this.defaultChildrenOptions;
        }
    };
    RoomComponent.prototype.onAdultsSelected = function (value) {
        this.room.get('adults').setValue(value);
    };
    RoomComponent.prototype.onChildSelected = function (value) {
        this.room.get('children').setValue(value);
    };
    return RoomComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Number)
], RoomComponent.prototype, "index", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */]) === "function" && _a || Object)
], RoomComponent.prototype, "room", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomComponent.prototype, "removeRoom", void 0);
RoomComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-room',
        template: __webpack_require__(439),
        styles: [__webpack_require__(429)]
    }),
    __metadata("design:paramtypes", [])
], RoomComponent);

var _a;
//# sourceMappingURL=room.component.js.map

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(24);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoomsFieldsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RoomsFieldsComponent = (function () {
    function RoomsFieldsComponent() {
        this.isOpen = false;
        this.close = this.close.bind(this);
        this.offClickHandler = this.offClickHandler.bind(this);
    }
    RoomsFieldsComponent.prototype.ngOnInit = function () {
        document.addEventListener('click', this.offClickHandler);
    };
    RoomsFieldsComponent.prototype.ngOnDestroy = function () {
        document.removeEventListener('click', this.offClickHandler);
    };
    RoomsFieldsComponent.prototype.toggle = function (evt) {
        if (this.isMobileDevice) {
            this.openFormModal();
            this.close();
        }
        else {
            this.isOpen = !this.isOpen;
        }
    };
    RoomsFieldsComponent.prototype.offClickHandler = function (event) {
        if (!this.roomsContainer.nativeElement.contains(event.target)) {
            this.close();
        }
    };
    RoomsFieldsComponent.prototype.close = function () {
        this.isOpen = false;
    };
    return RoomsFieldsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('roomsContainer'),
    __metadata("design:type", Object)
], RoomsFieldsComponent.prototype, "roomsContainer", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])('parentForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */]) === "function" && _a || Object)
], RoomsFieldsComponent.prototype, "parentForm", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])('totalRooms'),
    __metadata("design:type", Number)
], RoomsFieldsComponent.prototype, "totalRooms", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])('totalGuests'),
    __metadata("design:type", Number)
], RoomsFieldsComponent.prototype, "totalGuests", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])('canAddRooms'),
    __metadata("design:type", Object)
], RoomsFieldsComponent.prototype, "canAddRooms", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsFieldsComponent.prototype, "getRooms", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsFieldsComponent.prototype, "addRoom", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsFieldsComponent.prototype, "removeRoom", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsFieldsComponent.prototype, "isMobileDevice", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsFieldsComponent.prototype, "openFormModal", void 0);
RoomsFieldsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-rooms-fields',
        template: __webpack_require__(440),
        styles: [__webpack_require__(430)]
    }),
    __metadata("design:paramtypes", [])
], RoomsFieldsComponent);

var _a;
//# sourceMappingURL=rooms-fields.component.js.map

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(24);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoomsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RoomsComponent = (function () {
    function RoomsComponent() {
    }
    return RoomsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])('parentForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroup */]) === "function" && _a || Object)
], RoomsComponent.prototype, "parentForm", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsComponent.prototype, "canAddRooms", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsComponent.prototype, "getRooms", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsComponent.prototype, "addRoom", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], RoomsComponent.prototype, "removeRoom", void 0);
RoomsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-rooms',
        template: __webpack_require__(441),
        styles: [__webpack_require__(431)]
    })
], RoomsComponent);

var _a;
//# sourceMappingURL=rooms.component.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_service__ = __webpack_require__(274);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DATE_FORMAT = 'DD/MM/YYYY';
var MAX_ROOMS_COUNT = 9;
var TABLET_WIDTH = 768;
var SearchBarComponent = (function () {
    function SearchBarComponent(_fb, utils) {
        this._fb = _fb;
        this.utils = utils;
        this.totalRooms = 0;
        this.totalGuests = 0;
        this.showDialog = false;
        this.getRooms = this.getRooms.bind(this);
        this.addRoom = this.addRoom.bind(this);
        this.removeRoom = this.removeRoom.bind(this);
        this.openFormModal = this.openFormModal.bind(this);
    }
    SearchBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.searchForm = this._fb.group({
            location: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].minLength(5)]],
            lat: [''],
            long: [''],
            checkIn: [null, [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].required]],
            checkOut: [null, [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].required]],
            nights: [1],
            rooms: this._fb.array([
                this.initRoom(),
            ])
        });
        this.setTotals();
        this.getRooms().valueChanges.subscribe(function (rooms) {
            _this.setTotals();
        });
        this.getCheckout().valueChanges.subscribe(function (checkOut) {
            _this.setNights();
        });
    };
    SearchBarComponent.prototype.openFormModal = function () {
        this.showDialog = true;
    };
    SearchBarComponent.prototype.setTotals = function () {
        var rooms = this.getRooms().value;
        this.totalRooms = rooms.length;
        this.totalGuests = rooms.map(function (r) {
            return parseInt(r.adults, 10) + parseInt(r.children, 10);
        }).reduce(function (a, b) { return a + b; }, 0);
    };
    SearchBarComponent.prototype.setNights = function () {
        var checkIn = __WEBPACK_IMPORTED_MODULE_2_moment__(this.getCheckin().value, DATE_FORMAT);
        var checkOut = __WEBPACK_IMPORTED_MODULE_2_moment__(this.getCheckout().value, DATE_FORMAT);
        this.searchForm.get('nights').setValue(checkOut.diff(checkIn, 'd'));
    };
    SearchBarComponent.prototype.initRoom = function () {
        var room = this._fb.group({
            adults: [1, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].required],
            children: [0],
            accessible: [false]
        });
        return room;
    };
    SearchBarComponent.prototype.canAddRooms = function () {
        var rooms = this.getRooms();
        return rooms.length < MAX_ROOMS_COUNT;
    };
    SearchBarComponent.prototype.addRoom = function () {
        if (!this.canAddRooms()) {
            return;
        }
        var rooms = this.getRooms();
        rooms.push(this.initRoom());
    };
    SearchBarComponent.prototype.removeRoom = function (evt, i) {
        evt.stopPropagation();
        var rooms = this.getRooms();
        rooms.removeAt(i);
    };
    SearchBarComponent.prototype.getRooms = function () {
        return this.searchForm.get('rooms');
    };
    SearchBarComponent.prototype.getCheckout = function () {
        return this.searchForm.get('checkOut');
    };
    SearchBarComponent.prototype.getCheckin = function () {
        return this.searchForm.get('checkIn');
    };
    SearchBarComponent.prototype.onSubmit = function (formValue) {
        var url = 'https://www.travelodge.co.uk/search/results';
        var queryString = encodeURI(this.utils.toQueryString(formValue));
        window.location.href = url + queryString;
    };
    ;
    SearchBarComponent.prototype.isMobileDevice = function () {
        return window.innerWidth <= TABLET_WIDTH;
    };
    return SearchBarComponent;
}());
SearchBarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-search-bar',
        template: __webpack_require__(442),
        providers: [__WEBPACK_IMPORTED_MODULE_3__utils_service__["a" /* Utils */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__utils_service__["a" /* Utils */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__utils_service__["a" /* Utils */]) === "function" && _b || Object])
], SearchBarComponent);

var _a, _b;
//# sourceMappingURL=search-bar.component.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Utils; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Utils = (function () {
    function Utils() {
    }
    Utils.prototype.toQueryString = function (params) {
        var queryParams = Object.keys(params).map(function (key) {
            if (Array.isArray(params[key])) {
                return params[key].map(function (p, i) {
                    return Object.keys(p).map(function (ak) {
                        return key + "[" + i + "][" + ak + "]=" + p[ak];
                    }).join('&');
                }).join('&');
            }
            return key + "=" + params[key];
        });
        return queryParams.length > 0 ? "?" + queryParams.join('&') : '';
    };
    return Utils;
}());
Utils = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Injectable */])()
], Utils);

//# sourceMappingURL=utils.service.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 429:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)(false);
// imports


// module
exports.push([module.i, ".room {\n  position: relative;\n  margin-bottom: 10px;\n  padding: 5px 8px 0;\n  padding-right: 58px;\n  color: #000;\n  border: 1px solid #bdbdbd; }\n  @media (min-width: 576px) {\n    .room {\n      padding: 0;\n      border-top-width: 0;\n      border-right-width: 0;\n      border-left-width: 0; } }\n\n.room__row {\n  margin: 0 0 14px; }\n  .room__row:after {\n    display: table;\n    clear: both;\n    content: ''; }\n\n.room__title {\n  margin: 0;\n  font-family: 'fs_albert_light', sans-serif;\n  font-size: 18px;\n  color: #464646; }\n\n.room__select {\n  float: left;\n  box-sizing: border-box;\n  width: 50%;\n  font-size: 12px; }\n  .room__select select {\n    width: 100%; }\n\n.room__select:first-child {\n  padding-right: 4px; }\n\n.room__select:last-child {\n  padding-left: 4px; }\n\n.room input[type='checkbox'] {\n  position: absolute;\n  display: none;\n  width: 0;\n  height: 0;\n  margin: 0; }\n\n.room input[type=checkbox] + .checkbox-icon {\n  position: relative;\n  display: inline;\n  margin: 0;\n  padding: 0 0 0 22px;\n  line-height: 18px;\n  cursor: pointer; }\n  .room input[type=checkbox] + .checkbox-icon:before {\n    position: absolute;\n    top: -1px;\n    left: 0;\n    display: block;\n    box-sizing: border-box;\n    width: 18px;\n    height: 18px;\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAcCAMAAADcK/YTAAAACVBMVEX///8Ajcj///+EYD2FAAAAAXRSTlMAQObYZgAAAB5JREFUeAFjoBZgxAmQ1DDhAJSrGVUzqmZUDeE8CAALSQTlDAIdLQAAAABJRU5ErkJggg==);\n    background-repeat: no-repeat;\n    background-position: 50% 50%;\n    background-size: contain;\n    content: ''; }\n\n.room input[type=checkbox]:checked + .checkbox-icon:before {\n  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAcCAMAAADcK/YTAAAArlBMVEUAAAD///8BkMcBkMcBkMcBkMcBkMcBkMcBkMcBkMcBkMcBkMcBkMcBkMcBkMcAjcgAjsgBj8cBkMcDkccGksgHk8gKlMkPlsoVmcsZmswbm80cnM0kn88so9AzptI4qNNFrtZOsthpvd5wwN94xOGY0uik1+uq2uys2+2x3e674fC94vG+4/G/4/HB5PHI5/PQ6/XS6/Xb7/fd8Pjq9vrx+fz2+/35/P7+///////0rCFSAAAAD3RSTlMAACNbXmdpdHh7hYjD9P1qbxtKAAAAyklEQVR42q3K1xKCQAxA0WDFuhoL9t4b2M3//5gbZhAQN0/emft2wLIsELLLKWAjEcRKFpQxn+hKoMiQ+hDJFJkUQDIlJjZIps4kD5JZMMlBaCTCxkiqGZAME0yDZJg0axAzz3tUvOZMtipmHtPRLUJmPqG4mSAOrjHS3tGXWTUQu16U7Onb0LqJ6Bw/xDlQ0tBWo9YmIC4lDaMWYmMZkoTh9m1EHOs7Hv0wIWJyJrOhk6NJTxPBkOtg/0KSYTRkIphEygr6nzEVmjcKVHo5memHNwAAAABJRU5ErkJggg==); }\n\n.room .iDisabled {\n  display: inline-block;\n  width: 18px;\n  height: 18px;\n  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAaCAMAAABrajdMAAABUFBMVEX///8AjckBjckCjskEj8oFj8oGkMoHkMoIkcsJkcsMkswPlMwRlc0Slc0Vls0Wl84YmM4amc8fm9AinNAjndAmntEon9EroNIsodIyo9Q3ptU7p9U8qNY/qdZAqtdBqtdCq9dFrNhJrthLr9lPsNpVs9tcttxft91iud5jud5mu99nu99pvN9sveBtvuBvv+FxwOF2wuJ5w+N8xON9xeN+xeR/xuSBx+SDyOWFyOWGyeWHyeaKy+aLy+aNzOePzeeQzeeTz+iUz+ia0uqc0+qf1Oui1euj1uyl1+yo2O2r2e2s2u203e+43/C74fG84fG+4vG/4vHA4/LC5PLI5vPJ5/TL6PTN6fTP6vXR6vXS6/XT6/bZ7vfb7/fc7/je8Pjk8/nl8/nm9Prp9frq9vvs9/vt9/vx+fz2+/34/P75/P76/f77/f78/v79/v/+//+Ja58UAAAAAXRSTlMAQObYZgAAAP1JREFUeAFtz+c3QgEYx/HfF9Egm4QyItl77yF7j4iMEJH+/3fOPTfn1rn38/J7nhfPT4bvoUrvnOzWAY5kMwCwIJtdgCvZTUJcDm7hXQ5S8CQHb5CQgy+4kJMqImOGRxWJz8iP6VyWafrVRCBiSFp5B0bVxqJKffjoySlEQ6dpVqYlvGkpwj/Xq9mDxCQdrxXUMGF2D3sqMoUvIwOclix3syxDden9zzD1WUnqICbL/VaqnG3dvWgVz7PVo82/fTSGGVfGTzirggO4vAEISnEImT/nN1y059RV1nuSlzQP7sHD67OVVgikpeSDTPt1FEQzKva52V1LRctIwkp//10is29yLgAAAAAASUVORK5CYII=);\n  background-repeat: no-repeat;\n  background-position: 50% 50%;\n  background-size: contain;\n  vertical-align: middle; }\n\n.room-remove {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  width: 50px;\n  margin: 0;\n  padding: 0;\n  font-size: 34px;\n  color: #353535;\n  border: none;\n  background: #ccc;\n  cursor: pointer; }\n  .room-remove .hidden-xs {\n    display: none; }\n  @media (min-width: 576px) {\n    .room-remove {\n      bottom: auto;\n      width: auto;\n      font-family: 'fs_albert_light', sans-serif;\n      font-size: 14px;\n      text-decoration: underline;\n      background: transparent; }\n      .room-remove .visible-xs {\n        display: none; }\n      .room-remove .hidden-xs {\n        display: inline; }\n      .room-remove .iGuestX {\n        display: inline-block;\n        width: 21px;\n        height: 21px;\n        margin-left: 8px;\n        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAjhJREFUeNqUlc9LVFEUx+c9i8ZWlaVUgrpo2XJmFy1aVpO4sE2FqWEQZZL2AwuitIQUGg3aWKGR6/wD3LQJZhZtJFoUZIGa0o9VOjI0fY58nwyPO2+eFz6c9+4793vPPffe87xSqZSo1NLptIdJCns250Iul/ubiGieSzSVSjV4njfA43moh2X4BXvhoJ5fwRgTLFUVJboOzAR8g6d8n83n8ytl3w9gMnADDsEVhN9UFGXAA8xtGFQU/yJS42NuwhDcw/fx1kcTNVhyJ2xAJuiLA/7tUISzQd9mpMx6GP1PcJcZs4ltNsbfx1yDI4z/6au/Hz7DeMj5KGRCfRloDukOafP67CUQPWebwizho/AbXiNyQoInMSOwUu7EuKIC6trcKHJRi7Vzt99CdyztEuYOHId30IbfB4dfE+Yr1Po62EsuQbVJ+AIm9NwlqGgXlIKkr5uyUWkTlJK3UAfvq+yZBVbj6+olI3bWlnULnsBL3ndHiO6DookWoAHnOodgDWYaHupSrMKjCpM3ajUFE11TLk47fAc06aRuVzf0IHDM4dsK3/Fb95WzGehVVSpvc9AeHDXsR6s3sBiKcifmKryw9x3qt3xd1K3Ilm1S3rFx844o+7X0Z1uHH0erSL0mHr5BMa7oGd2onuBYhqvUsPJou52NUaWua5WD+I7EqacLSsUsA8L19JQEW+Ay32eqVn4G1iviC6r8P+AP7LHjp0M+BaOxKr/jH7XL7rPyb+lYR2gtKs//BRgA4ykb88VVgFYAAAAASUVORK5CYII=);\n        background-repeat: no-repeat;\n        background-position: 50% 50%;\n        background-size: contain;\n        vertical-align: middle; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)(false);
// imports


// module
exports.push([module.i, ".rooms {\n  position: relative;\n  color: #000; }\n\n.rooms__popup {\n  position: absolute;\n  z-index: 1000;\n  top: 100%;\n  right: 0;\n  box-sizing: border-box;\n  width: 420px;\n  margin-top: 10px;\n  padding: 10px;\n  color: #464646;\n  border: 1px solid #bdbdbd;\n  background: #fff; }\n  .rooms__popup:before, .rooms__popup:after {\n    position: absolute;\n    right: 50px;\n    border: solid 10px transparent;\n    border-top-width: 0;\n    content: ''; }\n  .rooms__popup:before {\n    z-index: 1;\n    top: -10px;\n    margin-right: -10px;\n    border-bottom-color: #bdbdbd; }\n  .rooms__popup:after {\n    z-index: 2;\n    top: -9px;\n    margin-right: -9px;\n    border-right-width: 9px;\n    border-bottom-width: 9px;\n    border-bottom-color: #fff;\n    border-left-width: 9px; }\n\n.btn-primary {\n  display: block;\n  margin: 10px auto;\n  font-size: 22px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)(false);
// imports


// module
exports.push([module.i, ".btn-secondary {\n  display: block;\n  width: 100%;\n  font-size: 22px;\n  margin-top: 20px; }\n  .btn-secondary .iAdd {\n    display: inline-block;\n    width: 9px;\n    height: 16px;\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAKCAMAAABR24SMAAAAG1BMVEUBkMgBkMgBkMgBkMgBkMgBkMgBkMgBkMgBkMi/vZLLAAAACHRSTlMAIEB/j7/P3y22DcsAAAAnSURBVHgBYwACJmZGBghg5mDCxeKAAlYGVlZWdg42VlYW/DoQJgMAKa4A0scAdIAAAAAASUVORK5CYII=);\n    background-repeat: no-repeat;\n    background-position: 50% 50%;\n    background-size: contain; }\n\n.importantInfo {\n  box-sizing: border-box;\n  margin-bottom: 20px;\n  padding: 10px;\n  border: 1px solid #ffa200;\n  border-radius: 2px;\n  background: #fff3dd; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 432:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)(false);
// imports


// module
exports.push([module.i, ".select {\n  box-sizing: border-box;\n  position: relative;\n  color: inherit; }\n  .select * {\n    box-sizing: border-box; }\n\n.select__selected,\n.select__option {\n  cursor: pointer;\n  line-height: 24px;\n  padding: 8px 10px;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n\n.select__selected {\n  background: #ffffff;\n  border: solid 1px #bdbdbd;\n  padding-right: 30px;\n  position: relative; }\n  .select__selected:after {\n    border-style: solid;\n    border-color: transparent;\n    border-top-color: #383c3d;\n    border-width: 8px 5px 0;\n    content: \"\";\n    position: absolute;\n    top: 50%;\n    right: 10px;\n    margin-top: -4px; }\n  .select__selected:hover:after, .select__selected:active:after, .select__selected:focus:after {\n    border-top-color: #3297fd; }\n\n.is-open .select__selected:after {\n  border-top-color: transparent;\n  border-bottom-color: #383c3d;\n  border-top-width: 0;\n  border-bottom-width: 8px; }\n\n.select__options {\n  background: #ffffff;\n  border: solid 1px #bdbdbd;\n  border-top: none;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);\n  color: #353535;\n  margin: -1px 0 0;\n  padding: 0;\n  list-style: none;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 100%;\n  z-index: 10; }\n\n.select__option {\n  border-top: solid 1px #bdbdbd; }\n\n.select__option:hover {\n  background: #008dc8;\n  color: #fff; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 433:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 123,
	"./af.js": 123,
	"./ar": 130,
	"./ar-dz": 124,
	"./ar-dz.js": 124,
	"./ar-kw": 125,
	"./ar-kw.js": 125,
	"./ar-ly": 126,
	"./ar-ly.js": 126,
	"./ar-ma": 127,
	"./ar-ma.js": 127,
	"./ar-sa": 128,
	"./ar-sa.js": 128,
	"./ar-tn": 129,
	"./ar-tn.js": 129,
	"./ar.js": 130,
	"./az": 131,
	"./az.js": 131,
	"./be": 132,
	"./be.js": 132,
	"./bg": 133,
	"./bg.js": 133,
	"./bm": 134,
	"./bm.js": 134,
	"./bn": 135,
	"./bn.js": 135,
	"./bo": 136,
	"./bo.js": 136,
	"./br": 137,
	"./br.js": 137,
	"./bs": 138,
	"./bs.js": 138,
	"./ca": 139,
	"./ca.js": 139,
	"./cs": 140,
	"./cs.js": 140,
	"./cv": 141,
	"./cv.js": 141,
	"./cy": 142,
	"./cy.js": 142,
	"./da": 143,
	"./da.js": 143,
	"./de": 146,
	"./de-at": 144,
	"./de-at.js": 144,
	"./de-ch": 145,
	"./de-ch.js": 145,
	"./de.js": 146,
	"./dv": 147,
	"./dv.js": 147,
	"./el": 148,
	"./el.js": 148,
	"./en-au": 149,
	"./en-au.js": 149,
	"./en-ca": 150,
	"./en-ca.js": 150,
	"./en-gb": 151,
	"./en-gb.js": 151,
	"./en-ie": 152,
	"./en-ie.js": 152,
	"./en-il": 153,
	"./en-il.js": 153,
	"./en-nz": 154,
	"./en-nz.js": 154,
	"./eo": 155,
	"./eo.js": 155,
	"./es": 158,
	"./es-do": 156,
	"./es-do.js": 156,
	"./es-us": 157,
	"./es-us.js": 157,
	"./es.js": 158,
	"./et": 159,
	"./et.js": 159,
	"./eu": 160,
	"./eu.js": 160,
	"./fa": 161,
	"./fa.js": 161,
	"./fi": 162,
	"./fi.js": 162,
	"./fo": 163,
	"./fo.js": 163,
	"./fr": 166,
	"./fr-ca": 164,
	"./fr-ca.js": 164,
	"./fr-ch": 165,
	"./fr-ch.js": 165,
	"./fr.js": 166,
	"./fy": 167,
	"./fy.js": 167,
	"./gd": 168,
	"./gd.js": 168,
	"./gl": 169,
	"./gl.js": 169,
	"./gom-latn": 170,
	"./gom-latn.js": 170,
	"./gu": 171,
	"./gu.js": 171,
	"./he": 172,
	"./he.js": 172,
	"./hi": 173,
	"./hi.js": 173,
	"./hr": 174,
	"./hr.js": 174,
	"./hu": 175,
	"./hu.js": 175,
	"./hy-am": 176,
	"./hy-am.js": 176,
	"./id": 177,
	"./id.js": 177,
	"./is": 178,
	"./is.js": 178,
	"./it": 179,
	"./it.js": 179,
	"./ja": 180,
	"./ja.js": 180,
	"./jv": 181,
	"./jv.js": 181,
	"./ka": 182,
	"./ka.js": 182,
	"./kk": 183,
	"./kk.js": 183,
	"./km": 184,
	"./km.js": 184,
	"./kn": 185,
	"./kn.js": 185,
	"./ko": 186,
	"./ko.js": 186,
	"./ky": 187,
	"./ky.js": 187,
	"./lb": 188,
	"./lb.js": 188,
	"./lo": 189,
	"./lo.js": 189,
	"./lt": 190,
	"./lt.js": 190,
	"./lv": 191,
	"./lv.js": 191,
	"./me": 192,
	"./me.js": 192,
	"./mi": 193,
	"./mi.js": 193,
	"./mk": 194,
	"./mk.js": 194,
	"./ml": 195,
	"./ml.js": 195,
	"./mn": 196,
	"./mn.js": 196,
	"./mr": 197,
	"./mr.js": 197,
	"./ms": 199,
	"./ms-my": 198,
	"./ms-my.js": 198,
	"./ms.js": 199,
	"./mt": 200,
	"./mt.js": 200,
	"./my": 201,
	"./my.js": 201,
	"./nb": 202,
	"./nb.js": 202,
	"./ne": 203,
	"./ne.js": 203,
	"./nl": 205,
	"./nl-be": 204,
	"./nl-be.js": 204,
	"./nl.js": 205,
	"./nn": 206,
	"./nn.js": 206,
	"./pa-in": 207,
	"./pa-in.js": 207,
	"./pl": 208,
	"./pl.js": 208,
	"./pt": 210,
	"./pt-br": 209,
	"./pt-br.js": 209,
	"./pt.js": 210,
	"./ro": 211,
	"./ro.js": 211,
	"./ru": 212,
	"./ru.js": 212,
	"./sd": 213,
	"./sd.js": 213,
	"./se": 214,
	"./se.js": 214,
	"./si": 215,
	"./si.js": 215,
	"./sk": 216,
	"./sk.js": 216,
	"./sl": 217,
	"./sl.js": 217,
	"./sq": 218,
	"./sq.js": 218,
	"./sr": 220,
	"./sr-cyrl": 219,
	"./sr-cyrl.js": 219,
	"./sr.js": 220,
	"./ss": 221,
	"./ss.js": 221,
	"./sv": 222,
	"./sv.js": 222,
	"./sw": 223,
	"./sw.js": 223,
	"./ta": 224,
	"./ta.js": 224,
	"./te": 225,
	"./te.js": 225,
	"./tet": 226,
	"./tet.js": 226,
	"./tg": 227,
	"./tg.js": 227,
	"./th": 228,
	"./th.js": 228,
	"./tl-ph": 229,
	"./tl-ph.js": 229,
	"./tlh": 230,
	"./tlh.js": 230,
	"./tr": 231,
	"./tr.js": 231,
	"./tzl": 232,
	"./tzl.js": 232,
	"./tzm": 234,
	"./tzm-latn": 233,
	"./tzm-latn.js": 233,
	"./tzm.js": 234,
	"./ug-cn": 235,
	"./ug-cn.js": 235,
	"./uk": 236,
	"./uk.js": 236,
	"./ur": 237,
	"./ur.js": 237,
	"./uz": 239,
	"./uz-latn": 238,
	"./uz-latn.js": 238,
	"./uz.js": 239,
	"./vi": 240,
	"./vi.js": 240,
	"./x-pseudo": 241,
	"./x-pseudo.js": 241,
	"./yo": 242,
	"./yo.js": 242,
	"./zh-cn": 243,
	"./zh-cn.js": 243,
	"./zh-hk": 244,
	"./zh-hk.js": 244,
	"./zh-tw": 245,
	"./zh-tw.js": 245
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 433;


/***/ }),

/***/ 435:
/***/ (function(module, exports) {

module.exports = "<div class=\"search-form__field field-check-in\">\n  <label class=\"field__label field__label--check-in\" for=\"checkIn\">Check in</label>\n  <label class=\"field__label field__label--date\" for=\"checkIn\">Date</label>\n  <div class=\"field__input field__input--datepicker\">\n    <span class=\"field__input-icon\">\n      <a class=\"\">\n        <i class=\"iCalendar\"></i>\n      </a>\n    </span>\n\n    <div class=\"input-holder\">\n      <app-datepicker\n        #checkInComponent\n        className=\"check-in-datepicker\"\n        title=\"When are you checking in?\"\n        controlName=\"checkIn\"\n        [defaultDate]=\"parentForm.get('checkIn').value\"\n        [minDate]=\"parentForm.get('checkIn').value\"\n        [(parentForm)]=\"parentForm\"\n        (click)=\"checkDeviceWidth($event)\">\n      </app-datepicker>\n    </div>\n  </div>\n</div>\n\n<div class=\"search-form__field field-check-out\" [formGroup]=\"parentForm\">\n  <label class=\"field__label field__label--check-out\" for=\"checkIn\">Check out</label>\n  <div class=\"field__input field__input--datepicker\">\n    <span class=\"field__input-icon\">\n      <a class=\"\">\n        <i class=\"iCalendar\"></i>\n      </a>\n    </span>\n\n    <div class=\"input-holder\">\n      <app-datepicker\n        #checkOutComponent\n        className=\"check-out-datepicker\"\n        title=\"When are you checking out?\"\n        controlName=\"checkOut\"\n        [defaultDate]=\"parentForm.get('checkOut').value\"\n        [minDate]=\"parentForm.get('checkIn').value\"\n        [(parentForm)]=\"parentForm\"\n        (click)=\"checkDeviceWidth($event)\">\n      </app-datepicker>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ 436:
/***/ (function(module, exports) {

module.exports = "<div class=\"datepicker\" [formGroup]=\"parentForm\" #container>\n  <input\n    type=\"text\"\n    (focus)=\"onFocus($event)\"\n    readonly\n    [formControlName]=\"controlName\" />\n\n  <div class=\"datepicker-container {{ className }}\" [ngClass]=\"{ 'is-open': isOpen }\"  (click)=\"$event.stopPropagation()\">\n    <div class=\"datepicker-title\">\n      {{ title }}\n    </div>\n\n    <div class=\"datepicker__buttons\" [ngSwitch]=\"controlName\">\n      <ng-container *ngSwitchCase=\"'checkIn'\">\n        <button\n          type=\"button\"\n          [ngClass]=\"{'is-active': checkInEquals(0)}\"\n          (click)=\"setCheckInDate(0)\"\n          class=\"datepicker__btn\">Today</button>\n        <button\n          type=\"button\"\n          [ngClass]=\"{'is-active': checkInEquals(1)}\"\n          (click)=\"setCheckInDate(1)\"\n          class=\"datepicker__btn\">Tomorrow</button>\n      </ng-container>\n\n      <ng-container *ngSwitchCase=\"'checkOut'\">\n        <button\n          type=\"button\"\n          [ngClass]=\"{'is-active': nightsEqual(1)}\"\n          (click)=\"setCheckOutDate(1)\"\n          class=\"datepicker__btn\">1 Night</button>\n        <button\n          type=\"button\"\n          [ngClass]=\"{'is-active': nightsEqual(2)}\"\n          (click)=\"setCheckOutDate(2)\"\n          class=\"datepicker__btn\">2 Nights</button>\n        <button\n          type=\"button\"\n          [ngClass]=\"{'is-active': nightsEqual(3)}\"\n          (click)=\"setCheckOutDate(3)\"\n          class=\"datepicker__btn\">3 Nights</button>\n      </ng-container>\n    </div>\n\n    <div class=\"datepicker-holder\" #datepicker></div>\n  </div>\n</div>\n"

/***/ }),

/***/ 437:
/***/ (function(module, exports) {

module.exports = "<div class=\"search-form__field field-location\" [formGroup]=\"parentForm\">\n  <label class=\"field__label\" for=\"location\">Destination</label>\n  <div class=\"field__input\">\n    <span class=\"field__input-icon\">\n      <a (click)=\"setCurrentLocation()\" class=\"use-location\">\n        <i class=\"iLocation\"></i>\n      </a>\n    </span>\n\n    <span class=\"input-holder\">\n      <input\n        #input\n        class=\"typeahead\"\n        type=\"text\"\n        id=\"location\"\n        formControlName=\"location\"\n        placeholder=\"Place, postcode or hotel name\"\n        aria-controls=\"location\"\n        (blur)=\"onLocationBlur($event)\"\n        autocomplete=\"off\"\n        spellcheck=\"false\"\n        dir=\"auto\"\n        />\n\n      <button *ngIf=\"this.parentForm.get('location').value.length > 0\"\n          (click)=\"clearInput()\"\n          type=\"button\"\n          class=\"field__clear-btn\">\n        X\n      </button>\n    </span>\n  </div>\n</div>\n"

/***/ }),

/***/ 438:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"visible\" class=\"modal-dialog-overlay\" (click)=\"close()\"></div>\n\n<div *ngIf=\"visible\" class=\"modal-dialog\">\n  <div class=\"modal-dialog__header\">\n    <button *ngIf=\"closable\" (click)=\"close()\" aria-label=\"Close\" class=\"modal-dialog__close-btn\">\n      X\n    </button>\n\n    <h2 class=\"modal-dialog__title\">\n      {{ title }}\n    </h2>\n  </div>\n  <div class=\"modal-dialog__body\" #container>\n    <ng-content></ng-content>\n  </div>\n</div>\n"

/***/ }),

/***/ 439:
/***/ (function(module, exports) {

module.exports = "<div class=\"room\" [formGroup]='room'>\n  <div class=\"room__row\">\n    <p class=\"room__title\">Room {{index + 1}}</p>\n\n    <button\n      class=\"room-remove\"\n      type=\"button\"\n      *ngIf=\"index > 0\"\n      (click)=\"removeRoom($event, index)\"\n    >\n      <span class=\"visible-xs\">&times;</span>\n      <span class=\"hidden-xs\">Cancel room <i class=\"iGuestX\"></i></span>\n    </button>\n  </div>\n\n  <div class=\"room__row\">\n    <div class=\"room__select\">\n      <app-select\n        [options]=\"adultOptions\"\n        [value]=\"room.get('adults').value\"\n        (select)=\"onAdultsSelected($event)\">\n      </app-select>\n    </div>\n    <div class=\"room__select\">\n      <app-select\n        [options]=\"childrenOptions\"\n        [value]=\"room.get('children').value\"\n        (select)=\"onChildSelected($event)\">\n      </app-select>\n    </div>\n  </div>\n\n  <div class=\"room__row\">\n    <label class=\"checkbox\">\n      <input formControlName='accessible' type=\"checkbox\" />\n      <span class=\"checkbox-icon\"></span>\n      <i class=\"iDisabled\"></i>\n      Accessible room\n    </label>\n  </div>\n</div>\n"

/***/ }),

/***/ 440:
/***/ (function(module, exports) {

module.exports = "<div\n  class=\"rooms\"\n  [formGroup]=\"parentForm\"\n  #roomsContainer\n>\n  <div class=\"search-form__field field-guests--display\"\n    (click)=\"toggle($event)\"\n  >\n    <label class=\"field__label\">Rooms and Guests</label>\n    <div class=\"field__input field__input--totals\">\n      <div class=\"total-block total__rooms\">\n        <span class=\"total-icon total-icon--rooms\"></span>\n        <span class=\"total-value\">{{ totalRooms }}</span>\n      </div>\n\n      <div class=\"total-block total__guests\">\n        <span class=\"total-icon total-icon--guests\"></span>\n        <span class=\"total-value\">{{ totalGuests }}</span>\n      </div>\n    </div>\n  </div>\n\n  <div\n    class=\"rooms__popup\"\n    *ngIf=\"isOpen\" formArrayName=\"rooms\"\n  >\n    <app-rooms\n      [parentForm]=\"searchForm\"\n      [canAddRooms]=\"canAddRooms\"\n      [getRooms]=\"getRooms\"\n      [addRoom]=\"addRoom\"\n      [removeRoom]=\"removeRoom\"\n    >\n    </app-rooms>\n\n    <button\n      type=\"button\"\n      class=\"btn btn-primary\"\n      (click)=\"toggle()\"\n    >\n      Done\n    </button>\n  </div>\n</div>\n"

/***/ }),

/***/ 441:
/***/ (function(module, exports) {

module.exports = "<app-room\n  *ngFor=\"let room of getRooms().controls; let i=index\"\n  class=\"room-fields\"\n  [index]=\"i\"\n  [room]=\"room\"\n  [removeRoom]=\"removeRoom\"\n>\n</app-room>\n\n<div\n  class=\"groupBookingInfo importantInfo\"\n  *ngIf=\"!canAddRooms\"\n>\n  This page does not support bookings for over 9 rooms.\n  Please head over to <a href=\"/group-bookings\" data-on-cancel-modal=\"true\" title=\"Group Booking\">our group booking page</a>.\n</div>\n\n<div>\n  <button\n    class=\"btn btn-secondary\"\n    type=\"button\"\n    (click)=\"addRoom()\"\n    *ngIf=\"canAddRooms\"\n  >\n    <i class=\"iAdd\"></i> Add another room\n  </button>\n</div>\n"

/***/ }),

/***/ 442:
/***/ (function(module, exports) {

module.exports = "<form class=\"search-form\" [formGroup]=\"searchForm\" novalidate (ngSubmit)=\"onSubmit(searchForm.value)\">\n  <app-location-field\n    [parentForm]=\"searchForm\"\n    class=\"location-field-component\">\n  </app-location-field>\n\n  <app-date-fields\n    [parentForm]=\"searchForm\"\n    (isMobile)=\"openFormModal()\"\n    class=\"inline-fields form-row form-row--dates\">\n  </app-date-fields>\n\n  <app-rooms-fields\n    [parentForm]=\"searchForm\"\n    [totalRooms]=\"totalRooms\"\n    [totalGuests]=\"totalGuests\"\n    [canAddRooms]=\"canAddRooms()\"\n    [getRooms]=\"getRooms\"\n    [addRoom]=\"addRoom\"\n    [removeRoom]=\"removeRoom\"\n    [isMobileDevice]=\"isMobileDevice()\"\n    [openFormModal]=\"openFormModal\"\n    class=\"form-row form-row--guests\">\n  </app-rooms-fields>\n\n  <div class=\"search-form__submit-btn-container\">\n    <button type=\"submit\" class=\"search-form__submit-btn\" aria-controls=\"bookingSearch_search\">\n      Search\n    </button>\n  </div>\n\n  <app-modal-dialog title=\"Search and book\" [(visible)]=\"showDialog\">\n    <div class=\"modal-search-form\">\n      <app-location-field\n        [parentForm]=\"searchForm\"\n        class=\"location-field-component\">\n      </app-location-field>\n\n      <app-date-fields\n        [parentForm]=\"searchForm\"\n        class=\"inline-fields form-row form-row--dates\">\n      </app-date-fields>\n\n      <app-rooms\n        [parentForm]=\"searchForm\"\n        [canAddRooms]=\"canAddRooms()\"\n        [getRooms]=\"getRooms\"\n        [addRoom]=\"addRoom\"\n        [removeRoom]=\"removeRoom\"\n      >\n      </app-rooms>\n\n      <div class=\"search-form__submit-btn-container\">\n        <button\n          type=\"submit\"\n          class=\"btn btn-primary search-form__submit-btn\"\n          aria-controls=\"bookingSearch_search\"\n        >\n          Search\n        </button>\n      </div>\n    </div>\n  </app-modal-dialog>\n</form>\n"

/***/ }),

/***/ 443:
/***/ (function(module, exports) {

module.exports = "<div #container class=\"select\" [ngClass]=\"{'is-open': isOpen}\" (click)=\"allowEventPropagation($event)\">\n  <div class=\"select__selected\" (click)=\"toggle()\">\n    {{selected.label}}\n  </div>\n\n  <ul class=\"select__options\" *ngIf=\"isOpen\">\n    <li\n      class=\"select__option\"\n      *ngFor=\"let option of options\"\n      (click)=\"selectItem(option)\"\n    >\n      {{option.label}}\n    </li>\n  </ul>\n</div>\n"

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalDialogComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var $ = window['$'];
var ModalDialogComponent = (function () {
    function ModalDialogComponent() {
        this.closable = true;
        this.visibleChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]();
    }
    ModalDialogComponent.prototype.ngOnChanges = function () {
        this.visibleChange.emit(this.visible);
        $('body').toggleClass('modal-open', this.visible);
    };
    ModalDialogComponent.prototype.close = function () {
        this.visible = false;
        this.visibleChange.emit(this.visible);
    };
    return ModalDialogComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('container'),
    __metadata("design:type", Object)
], ModalDialogComponent.prototype, "container", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Object)
], ModalDialogComponent.prototype, "closable", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Boolean)
], ModalDialogComponent.prototype, "visible", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", String)
], ModalDialogComponent.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]) === "function" && _a || Object)
], ModalDialogComponent.prototype, "visibleChange", void 0);
ModalDialogComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-modal-dialog',
        template: __webpack_require__(438),
    })
], ModalDialogComponent);

var _a;
//# sourceMappingURL=modal-dialog.component.js.map

/***/ }),

/***/ 492:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(259);


/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(8);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Option; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Option = (function () {
    function Option(value, label) {
        this.value = value;
        this.label = label;
    }
    return Option;
}());

var SelectComponent = (function () {
    function SelectComponent() {
        this.select = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]();
        this.isOpen = false;
        this.selected = {
            value: null,
            label: ''
        };
        this.select = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]();
        document.addEventListener('click', this.offClickHandler.bind(this));
    }
    SelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selected = this.options.find(function (i) {
            return i.value === _this.value;
        });
    };
    SelectComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (changes['options'] && !changes['options'].firstChange) {
            var optionAvailable = changes['options'].currentValue.find(function (option) {
                return option.value === _this.selected.value;
            });
            if (typeof optionAvailable === 'undefined') {
                this.selected = this.options[0];
            }
        }
    };
    SelectComponent.prototype.toggle = function () {
        this.isOpen = !this.isOpen;
    };
    SelectComponent.prototype.offClickHandler = function (event) {
        if (!this.container.nativeElement.contains(event.target)) {
            this.isOpen = false;
        }
    };
    SelectComponent.prototype.allowEventPropagation = function (event) {
        if (!this.container.nativeElement.contains(event.target)) {
            event.stopPropagation();
        }
    };
    SelectComponent.prototype.selectItem = function (option) {
        this.isOpen = false;
        this.selected = option;
        this.select.emit(option.value);
    };
    return SelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('container'),
    __metadata("design:type", Object)
], SelectComponent.prototype, "container", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Array)
], SelectComponent.prototype, "options", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Input */])(),
    __metadata("design:type", Number)
], SelectComponent.prototype, "value", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]) === "function" && _a || Object)
], SelectComponent.prototype, "select", void 0);
SelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-select',
        template: __webpack_require__(443),
        styles: [__webpack_require__(432)]
    }),
    __metadata("design:paramtypes", [])
], SelectComponent);

var _a;
//# sourceMappingURL=select.component.js.map

/***/ })

},[492]);
//# sourceMappingURL=main.bundle.js.map